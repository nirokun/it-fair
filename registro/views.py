from django.shortcuts import render, redirect
from .models import Puntaje
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate

# Create your views here.

def index(request):
    return render(request,'index.html',{'elementos':Puntaje.objects.all})

def login(request):
    return render(request,'registration/login.html',{})

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form=UserCreationForm()
    
    context ={'form':form}
    return render(request,'registration/register.html',context)

def recuperar(request):
    return render(request,'recuperar.html',{})
    
def crear(request):
    nick = request.POST.get('nick','')
    puntaje = request.POST.get('puntaje',0)
    score = Puntaje(nick=nick,puntaje=puntaje)
    score.save()
    return redirect('index')

def juego(request):
    return render(request,'juego.html',{})
    
def buscar(request,id):
    score = Puntaje.objects.get(pk=id)
    return HttpResponse('Nick : '+score.nick)

def editar(request,id):
    score = Puntaje.objects.get(pk=id)
    return render(request,'editar.html',{'score':score})

def editado(request,id):
    score = Puntaje.objects.get(pk=id)
    nick = request.POST.get('nick','')
    puntaje = request.POST.get('puntaje',0)
    score.nick=nick
    score.puntaje=puntaje
    score.save()
    return redirect('index')

def eliminar(request,id):
    score = Puntaje.objects.get(pk=id)
    score.delete()
    return redirect('index')



