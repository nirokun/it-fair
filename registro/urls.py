from django.urls import path
from . import views

urlpatterns = [
    path('',views.index, name="index"),
    path('login/',views.login,name="login"),
    path('register/',views.register,name="register"),
    path('recuperar/',views.recuperar,name="recuperar"),
    path('juego/',views.juego,name="juego"),
    path('juego/crear',views.crear,name="crear"),
    path('juego/buscar/<int:id>',views.buscar, name="buscar"),
    path('juego/editar/<int:id>',views.editar, name="editar"),
    path('juego/editado/<int:id>',views.editado, name="editado"),
    path('juego/eliminar/<int:id>',views.eliminar, name="eliminar")
]